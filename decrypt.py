import sys
import base64

"""
The theory is the following:
- Given two lines of a text message `l1` and `l2`
- These two lines are encrypted with the same key `key`
- As XOR is commutative, we know have the relation:
    -> encrypt(l1, key) XOR encrypt(l2, key) == l1 XOR l2
- We can then try to guess words present either in l1 or l2:
- Let's say that encrypt(l1, key) XOR encrypt(l2, key) == abcdef123456
- Let's also say that our guess word in hex is 'ffeecc123', then:
    ->     abcdef123456
       XOR ffeecc123
       ________________
           abc211fef
- If 'abc211fef575' is ASCII is a human readable word, it is present 
in either l1 or l2
- We just have to repeat the opration trying to guess words in the sentences
"""

def xor_two_str(a, b):
    return "".join(["%x" % (int(x, 16) ^ int(y, 16)) for (x, y) in zip(a, b)])


def decrypt_file(filename, guess, ln1, ln2):
    line1 = ""
    line2 = ""

    # we read the two lines given as arguments and convert them to hex
    with open(filename, "r") as fp:
        for i, line in enumerate(fp):
            if i == int(ln1):
                line1 = base64.b64decode(line).encode('hex')
            elif i == int(ln2):
                line2 = base64.b64decode(line).encode('hex')

    # we xor these two lines
    xor = xor_two_str(line1, line2)

    # and we xor `xor` with the `guess` word at position 0
    print(xor_two_str(xor, guess).decode('hex')[:len(guess)])

    """ This loop may be useful for first words tries """
    # while len(guess) < len(xor):
    #    print(xor_two_str(xor, guess).decode('hex')[:len(guess)])
    #    ascii_guess = guess.decode('hex')
    #    guess = ascii_guess.rjust(len(ascii_guess) + 1, " ").encode('hex')


def main(argv):
    if len(argv) < 5:
        sys.stderr.write(
            "Usage : %s <file1> <guess_word> <line_number_1> <line_number_2> \n" % (argv[0]))
        return 1

    decrypt_file(argv[1], argv[2].encode('hex'), argv[3], argv[4])


if __name__ == "__main__":
    sys.exit(main(sys.argv))
